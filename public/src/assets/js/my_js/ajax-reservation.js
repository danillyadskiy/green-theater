$(document).ready(function(){

    function reservation(){
        $('.step-content').hide();
        var url = 'index/reservation';
        var t_d = timeDayStartEnd();
        var day_start = t_d.day_start;
        var day_end = t_d.day_end;
        var time_start = t_d.time_start;
        var time_end = t_d.time_end;
        var month = $('.ui-datepicker-month').html();
        var year = $('.ui-datepicker-year').html();
        //Add place---------------
        var add_place = 'false';
        if ($('#add_place').is('[checked]')){
            add_place = 'true';
        }
        //-----------------------
        var data = {
            'day_start'        : day_start,
            'day_end'          : day_end,
            'time_start'       : time_start,
            'time_end'         : time_end,
            'month'            : month,
            'year'             : year,
            'add_place'        : add_place
        };
            $.ajax({
                beforeSend: function () {
                    $('.be-loading').addClass('be-loading-active');
                },
                complete: function () {
                    $('.be-loading').removeClass('be-loading-active');
                },
                type: 'POST',
                url: url,
                data: data
            }).done(function (response) {
                console.log(response);
                //Обработка ошибки
                if (response === 'fail'){
                    var message = 'Выбранный вами диапазон времени уже занят'
                    $('.alert-warning').find('.message').html(message);
                    $('.alert-warning').show();
                    $('li[data-step="2"]').trigger('click');
                    return true;
                }else{
                    $("#id_in").attr('value', response);
                    $('.step-content').slideDown("normal");
                    return true;
                }
            }).fail(function () {
                ajaxConnectionFail();
                return false;
            });
            return false;
    };

    function timeDayStartEnd() {
        var day_start = parseInt($('.ui-state-active').html());
        var day_end = day_start;
        var time_start = parseInt($(".block_hours").find(".active").find("span").html());
        var duration = parseInt($(".durations").find(".active span").html().split(' ')[1]);
        var time_end = time_start + duration;
        if (time_end > 23){
            time_end = time_end - 24;
            day_end = day_end + 1;
        }
        return {
                day_start: day_start, 
                day_end: day_end, 
                time_start: time_start, 
                time_end: time_end
               };
    }

    $('#page_3').click(function(){
        reservation();
    });
});