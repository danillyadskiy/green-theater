$(document).ready(function(){

    function GetTime2(){
        $('.durations').html('');
        $('.step-content').hide();
        $('.total_price').html("ИТОГО 0 ₽");
        $('#add_place').next('span').html("Дополнительное место 0 ₽");
        $('#add_place').attr('disabled', 'dissabled');
        $('#add_place').removeAttr('checked');
        $("#add_place").prop('checked', false);
        var url = 'index/getTime';
        var day = $('.ui-state-active').html();
        var month = $('.ui-datepicker-month').html();
        var year = $('.ui-datepicker-year').html();
        var data = {
            'day'              : day,
            'month'            : month,
            'year'             : year
        };
            $.ajax({
                beforeSend: function () {
                    $('.be-loading').addClass('be-loading-active');
                },
                complete: function () {
                    $('.be-loading').removeClass('be-loading-active');
                },
                type: 'POST',
                url: url,
                data: data
            }).done(function (response) {
                duration(response.time_1, response.time_2);

                $('.step-content').slideDown("normal");
                return true;
                
            }).fail(function () {
                ajaxConnectionFail();
                return false;
            });
            return false;
        //}
    };

    function DurationRules(time1, time2){
        var activeTime = $(".hours").find($(".active")).find('span').html();
        var count = null;
        activeTime = parseInt(activeTime);
        //Проверка возможность бронирования в сегодняшний день
        if (time1){
            for(var i=0; i<time1.length; i++) {
                if (activeTime < time1[i]){
                    count = time1[i]-activeTime-1;
                    break;
                }
            }
        }   
        //Проверка возможность бронирования с переносом на след. день
        if (time2 && !count){
            for(var i=0; i<time2.length; i++) {
                if (activeTime < 24+time2[i]){
                    count = 24+time2[i]-activeTime-1;
                    break;
                }
            }
        }
        //Если новая пустая страница
        if(!count){
            count = 8;
        }
        //Больше 8 нельзя
        if (count > 8){
            count = 8;
        }
        return count;
    }

    function UppendDurations(price){
        //Понадобится для сложения элементов массива
        const reducer = (accumulator, currentValue) => accumulator + currentValue;
        var total_price = price.reduce(reducer);
        total_price = total_price.toString();
        var a = total_price.slice(0,total_price.length - 3);
        var b = total_price.substring(total_price.length-3,total_price.length);
        var text = '';
        if (price.length < 5){text = 'часа';}
            else {text = 'часов';}
        $('.durations').append(
            "<div class='duration'>"+
                "<span> "+price.length+" "+text+" - "+a+" "+b+" ₽</span>"+
            "</div>"
        );
        return true;
    }

    function duration(time1, time2){
        //Инициализация
        var count = DurationRules(time1, time2);
        var hours = $(".hours").find($(".active")).parent();
        var hour = $(".hours").find($(".active"));
        var type = [], price = [], time = [];
        var change_time = '.leg_time_1';
        //Заносим в массив элементы текущей даты
        type.push(hour.attr("class").split(' ')[1]);
        time.push(parseInt(hour.find('span').html()));
        $('.leg_time_1').find('.Legend').each(function() {
            if (type == $(this).attr('class').split(' ')[1]){
                price.push(parseInt($(this).next('span').html().split(' ')[1]));
            }
            
        });

        //Заносим в массив элементы всех последующих дат, меньше 2-х нельзя
        for(i=0; i<count-1; i++){
            hour = hours.nextAll().eq(i).find("div");
            type.push(hour.attr("class").split(' ')[1]);
            time.push(parseInt(hour.find('span').html()));
            //Переход по дням недели, соотв. изменение цены на часы
            //Для этого был введен массив time
            if (time[i+1]<time[i]){
                change_time = '.leg_time_2';
            }
            //В зависимости от дня недели вытаскиваем цены
            $(change_time).find('.Legend').each(function() {
                if (type[i+1] == $(this).attr('class').split(' ')[1]){
                    price.push(parseInt($(this).next('span').html().split(' ')[1]));
                }
                
            });

            //В цикле выводим все возможные варианты
            UppendDurations(price);

            /*console.log(price);
            console.log(type);
            console.log(time);*/
        }
        return true;
    }

    $('#page_2').click(GetTime2);
    $('.steps').find('li[data-step="3"]').click(function(){
        if($(this).hasClass('complete')){
            GetTime2();
        }
    });
});