$(document).ajaxComplete(function(){
  $('#page_2').attr('disabled', 'disabled');
  $('.hour').click(function(){
    if (!$(this).hasClass('Disabled')){
        $('.hour').each(function() {
          if ($(this).hasClass('active')){
            $(this).removeClass('active');
            color = $(this).find('span').css('color'),
            background = $(this).css('background-color'); 
            $(this).css('background', color);
            $(this).find('span').css('color', background);
          }
        });
        color = $(this).find('span').css('color'),
        background = $(this).css('background-color'); 
        $(this).addClass('active');
        OneHour();
        $(this).css('background', color);
        $(this).find('span').css('color', background);
    }
  });

  function OneHour(){
    var active = $(".hours").find($(".active")).parent();
    var message = 'Допустимое время брони от 2 часов';
    if (active.next().find($(".hour")).hasClass("Disabled")){
        $('.alert-warning').find('.message').html(message);
        $(".alert-warning").slideDown('normal');
        $('#page_2').attr('disabled', 'disabled');
        return true;
    }else{
        //Scroll
        if($(window).scrollTop() + $(window).height() != $(document).height()) {
            $("html, body").animate({ scrollTop: $(document).height()-$(window).height() }, 500);
        }
        $(".alert-warning").slideUp('normal');
        $('#page_2').removeAttr('disabled');
        return true;
    }
  }

  function clear_duration(){
    $('.duration').each(function() {
        if ($(this).hasClass('active')){
          $(this).removeClass('active');
          color = $(this).find('span').css('color'),
          background = $(this).css('background-color'); 
          $(this).css('background', color);
          $(this).find('span').css('color', background);
        }
      });
    $('#page_3').attr('disabled', 'disabled');
    return true;
  }
  function clear_datepicker(){
    $("#page_1").attr('disabled', 'disabled'); 
    $("#datepicker").find(".ui-state-active").removeClass("ui-state-active");
    return true;
  }

  $('.steps').find('li[data-step="1"]').click(function(){
      clear_duration();
      //clear_datepicker();
  });
  $('#page_2_back').click(function(){
      clear_duration();
      //clear_datepicker();
  });
});