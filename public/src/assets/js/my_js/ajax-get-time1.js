
    function GetTime1(){
        $('.step-content').hide();
        $('.block_hours').html('');
        $('.block-legend').html('');
        var url = 'index/getTime';
        var day = $('.ui-state-active').html();
        var month = $('.ui-datepicker-month').html();
        var year = $('.ui-datepicker-year').html();
        var data = {
            'day'              : day,
            'month'            : month,
            'year'             : year
        };
            $.ajax({
                beforeSend: function () {
                    $('.be-loading').addClass('be-loading-active');
                },
                complete: function () {
                    $('.be-loading').removeClass('be-loading-active');
                },
                type: 'POST',
                url: url,
                data: data
            }).done(function (response) {
                // Выводим сразу 2 даты, но одну скрытую, другую нет
                //Для того, чтобы срабатывал ALERT
                //Отправляем запрос по текущей дате
                MakeLegend($('.ui-state-active').html(), '', 'time_1');
                var response_time = AddExtraTime(response.time_1, 'time_1');
                MakeBlockHours(response_time);
                //Отправляем запрос по следующей дате
                //Отправляем следующую дату
                MakeLegend(parseInt($('.ui-state-active').html())+1,'none', 'time_2');
                var response_time = AddExtraTime(response.time_2, 'time_2');
                MakeBlockHours(response_time, 'none');

                $('.step-content').slideDown("normal");
                return true;
                
            }).fail(function () {
                ajaxConnectionFail();
                return false;
            });
            return false;
        //}
    };

    function AddExtraTime(time, type) {
        var time_extra = [];

        //Смотрим, есть ли на след. день бронь на 00:00
        //Если есть - блокируем 23:00 текущего дня
        if (time && type == 'time_2'){
            if (time[0] == 0){
                Change23ToDisabled();
            }
        }

        if(time){
            for(var i=0; i<time.length; i++) {
                //Блокируем час на уборку
                if ((time[i+1] - time[i]) >= 2){
                    time_extra.push(time[i+1]-1);
                }
                //Убираем 2 часа перед началом сеанса
                // = не позволяет иметь окно на 1 час
                if ((time[i+1] - time[i]) == 3){
                    time_extra.push(time[i+1]-2);
                }
                //то же самое только для первого элемента массива
                if (i == 0){
                    if (time[0] >= 1){
                        time_extra.push(time[i]-1);
                    }
                }

            }
            time = $.merge(time,time_extra);
        }else{
            var time = [];
        }
        //Блокируем все даты раньше текущего времени
        if (type == 'time_1'){
            var choosen_date = ParseDate();
            var current_date = new Date($.now());
            if (D_M_Y(choosen_date) == D_M_Y(current_date)){
                var hours = parseInt(new Date($.now()).getHours());
                for(var i=0; i<=hours; i++) {
                    time.push(i);
                } 
            }
        }
        if (time){
            //Удаляем дубликаты из массива
            time = time.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            });
        
            //Сортируем массив
            time.sort(function (a, b) {
                return a - b;
            });
        }
        return time;
    };

    function D_M_Y(date) {
        return date.getDate()+":"+date.getMonth()+":"+date.getFullYear();
    }

    function ParseDate(day=$('.ui-state-active').html()) {
        var month = $('.ui-datepicker-month').html();
        var year = $('.ui-datepicker-year').html();
        var month_list = {
            "Январь" : "00",
            "Февраль" : "01",
            "Март" : "02",
            "Апрель" : "03",
            "Май" : "04",
            "Июнь" : "05",
            "Июль" : "06",
            "Август" : "07",
            "Сентябрь" : "08",
            "Октябрь" : "09",
            "Ноябрь" : "10",
            "Декабрь" : "11",
        };
        month = month_list[month];
        return new Date(year,month,day);
    }

    function MakeLegend(day, display, class_time){
        var date = ParseDate(day);
        var days = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
        day = days[date.getDay()];
        if (day == 'пт' || day == 'сб' || day == 'вс'){
            var price = ['2500', '3000', '4000'];
        }else{
            var price = ['2000', '2500', '3000'];
        }
        $('.block-legend').append(
            "<div class='row_legenda leg_"+class_time+"' style='display:"+display+"'>"+
                "<span class='Legend Type1'></span>"+
                "<span class='span_height span_margin'> "+price[0]+" ₽ </span>"+
                "<span class='Legend Type2'></span>"+
                "<span class='span_height span_margin'> "+price[1]+" ₽ </span>"+
                "<span class='Legend Type3'></span>"+
                "<span class='span_height span_margin'> "+price[2]+" ₽ </span>"+        
                "<span class='Legend Disabled'></span>"+
                "<span class='span_disabled'> занято</span>"+
            "</div>"
            );
        return true;
    };
    
    function MakeBlockHours(response_time, display=''){
        for(i=0; i<=23; i++){ 
            if (i == 23 || (i>=0 && i<8))
                type = 'Type3';
            if (i>=15 && i<23)
                type = 'Type2';
            if (i>=8 && i<15)
                type = 'Type1';
            if(i<10){
                time = '0' + i + ':00';
            }else{
                time = i + ':00';
            }
            if (response_time){
                response_time.forEach(element => {
                    if(element == i){
                        type = 'Disabled';
                    }
                });
            }
            $('.block_hours').append(
            "<div class='hours' style='display:"+display+"'>"+
                "<div class='hour "+type+" '>"+
                      "<span>"+time+"</span>"+
                "</div>"+
            "</div>"
            );
         }
         return true;
    }

    /*Функция для отключения 23:00, если на след день есть бронь с 00:00*/
    function Change23ToDisabled() {
        $(".block_hours span:contains('23:00')").parent().removeClass('Type3');
        $(".block_hours span:contains('23:00')").parent().addClass('Disabled');
	    return true;
    }

    //Здесь, потому, что должен срабатывать вне ajax
    $('#page_1').click(function(){
        day = $('.ui-state-active').html();
        month = $('.ui-datepicker-month').html();
        year = $('.ui-datepicker-year').html();
        $('.d_m_y').html(day+"/"+month+"/"+year);
        $(".alert").hide();
    });

    $('#page_1').click(GetTime1);
    $('.steps').find('li[data-step="2"]').click(function(){
        if($(this).hasClass('complete')){
            cansel_res();
            GetTime1();
        }
    });

    $('#page_3_back').click(GetTime1);
