function ajaxConnectionFail() {
    var message = '<span class="span-message">Что-то пошло не так. Телефон администратора </span>';
    $('.alert-index .span-message').remove();
    $('.alert-index').addClass('alert-danger');
    $('.alert-index').find('button').after(message);
    $('.alert-index .icon').find('.mdi').attr('class', 'mdi');
    $('.alert-index .icon').find('.mdi').addClass('mdi-close-circle-o');
    $('.alert-index').show();
    $('li[data-step="1"]').trigger('click');
    return true;
}
