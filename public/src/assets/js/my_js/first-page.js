$(document).ready(function(){
    $('.close').click(function() {
        $(".alert").slideUp('normal');
    });

    function stepContentSlider(){
        $('.step-content').hide();
        setTimeout(function () {
            $('.step-content').slideDown("normal");
        },1);
    }
    $('.steps').find('li[data-step="1"]').click(function() {
        $('.be-loading').removeClass('be-loading-active');
        stepContentSlider(); 
    });
    $('#page_2_back').click(stepContentSlider);  
});