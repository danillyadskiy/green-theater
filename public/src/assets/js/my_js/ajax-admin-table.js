$(document).ready(function() {
    $('#Table').DataTable({
        //serverSide: true,
        ajax: {
            url: '/admin/index',
            method: 'POST',
        },
        scrollX: true,
        language: {
          "processing": "Подождите...",
          "search": "Поиск:",
          "lengthMenu": "Показать _MENU_ записей",
          "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
          "infoEmpty": "Записи с 0 до 0 из 0 записей",
          "infoFiltered": "",
          "infoPostFix": "",
          "loadingRecords": "Загрузка записей...",
          "zeroRecords": "Записи отсутствуют",
          "emptyTable": "В таблице отсутствуют данные",
          "paginate": {
            "first": "Первая",
            "previous": "Предыдущая",
            "next": "Следующая",
            "last": "Последняя"
          },
          "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {data: "id", searchable: false},
            {data: "abonement_id"},
            {
            data: "created_at",
            render: function(data, type, row){
                if(type === "sort" || type === "type"){
                    return data;
                }
                return moment(data).format("DD-MM-YYYY / HH:mm");
                }
            },
            {data: "status", searchable: false},
            {data: "name"},
            {data: "phone"},
            {data: "email"},
            {data: "duration"},
            {data: "date"},
            {data: "time"},
            {data: "amount"},
            {data: "add_place", searchable: false},
            {data: "payment_id"},
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function ( data, type, row ) {
                    if (data['status'] == 2){
                        return '<span class="icon" onClick = "sendRepeatMail(this);">'+
                        '<i class="mdi mdi-email"></i></span>';
                    }else{
                        return '<span class="icon" disabled="disabled">'+
                        '<i class="mdi mdi-email mdi-disabled"></i></span>';
                    }
                }
            }
        ],
        createdRow: function (row, data) {
            //ID
                $('td', row).eq(0).css('color', '#8b8b8b');
            //Status
            if (data['status'] == 2) {
                $('td', row).eq(3).text('Оплачено');
                $('td', row).eq(3).css('color', '#12b7a9');
            }else if (data['status'] == 1 || data['status'] == -1){
                $('td', row).eq(3).text('Бронь');
                $('td', row).eq(3).css('color', '#eb6357');
            }
            //Add_Place
            if (data['add_place'] == 'true') {
                $('td', row).eq(11).text('ДА');
                $('td', row).eq(11).css('color', '#12b7a9');
            }else{
                $('td', row).eq(11).text('НЕТ');
                $('td', row).eq(11).css('color', '#eb6357');
            }
            //PayMaster ID
            if (data['payment_id']) {
                $('td', row).eq(12).css('color', '#12b7a9');
            }else{
                $('td', row).eq(12).text('не оплачено');
                $('td', row).eq(12).css('color', '#eb6357');
            }
        }
    });
});

function sendRepeatMail(row) {
    if (confirm('Выслать повторный email?')) {
        var url = 'admin/sendMail';
        var data = {
            'id'  : $(row).parents('tr').find('td').eq(0).text(),
        };
        $.ajax({
            beforeSend: function () {
                $('.be-loading').addClass('be-loading-active');
            },
            complete: function () {
                $('.be-loading').removeClass('be-loading-active');
            },
            type: 'POST',
            url: url,
            data: data
        }).done(function () {
            alert('Email отправлен!');
            return true;
        }).fail(function () {
            alert('Ошибка!');
            return false;
        });
        return false;
    }
}