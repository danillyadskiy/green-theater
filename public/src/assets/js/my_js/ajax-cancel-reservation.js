
    function canselReservation(){
        $('.step-content').hide();
        var url = 'index/cancelRes';
        var id = $("#id_in").attr('value');
        var data = {
            'id'  : id
        };
            $.ajax({
                beforeSend: function () {
                    $('.be-loading').addClass('be-loading-active');
                },
                type: 'POST',
                url: url,
                async: false,
                data: data
            }).done(function (response) {
                console.log(response);
                 return true;
            }).fail(function () {
                ajaxConnectionFail();
                return false;
            });
            return false;
    };


    function cansel_res(){
        //Убеждаемся, что мы на четвертом шаге и сущестует value
        if($('.step-content').find('.step-pane.active').attr('data-step') === "4" &&
        $("#id_in").attr('value')){
            canselReservation();
            $("#id_in").attr('value', '');
        }
    }
    //Не учитывает при перезагрузке страницы
    $('.logo-container a').click(function() { window.onbeforeunload = null; });
    $('form').submit(function() { window.onbeforeunload = null; });
    $('.overtime-ok-btn').click(function() { window.onbeforeunload = null; });
    $('#ask-phone').click(function() { window.onbeforeunload = null; });
    // Действие на перезагрузку страницы
    window.onbeforeunload = function() {
        cansel_res();
    };
    //На другие действия
    $('.steps').find('li[data-step="1"]').click(cansel_res);
    $('.steps').find('li[data-step="3"]').click(cansel_res);
    $('.logo-container a').click(cansel_res);


