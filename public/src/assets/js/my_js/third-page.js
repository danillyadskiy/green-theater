$(document).ajaxComplete(function(){
    $('#page_3').attr('disabled', 'disabled');
    $('.duration').click(function(){
      //Scroll
      if($(window).scrollTop() + $(window).height() != $(document).height()) {
          $("html, body").animate({ scrollTop: $(document).height()-$(window).height() }, 500);
      }

      $('.duration').each(function() {
        if ($(this).hasClass('active')){
          var price = $(this).html().split('- ')[1].split(' ₽')[0];
          price = price.replace(' ', '');
          $(this).removeClass('active');
          color = $(this).find('span').css('color'),
          background = $(this).css('background-color'); 
          $(this).css('background', color);
          $(this).find('span').css('color', background);
          $('.total_price').html("ИТОГО 0 ₽");;
        }
      });
      var price = $(this).html().split('- ')[1].split(' ₽')[0];
      price = price.replace(' ', '');
      color = $(this).find('span').css('color'),
      background = $(this).css('background-color'); 
      $(this).addClass('active');
      $('#page_3').removeAttr('disabled');
      $(this).css('background', color);
      $(this).find('span').css('color', background);

      ten_persent_place(price);
      if ($('#add_place').is('[checked]')){
        var price_add_place = additional_place();
        parse_price(price_add_place, 'up');
      }
      parse_price(price, 'up');

    });

    $('#page_2').click(function(){
        h_m = $('.block_hours').find('.active').find('span').html();
        $('.h_m').html(h_m);
    });

    //заполняем hidden инпуты
    $('#page_3').click(function(){
        //Сумма
        var total_price = $(".total_price").html().split(' ')[1];
        total_price += $(".total_price").html().split(' ')[2];
        total_price = parseInt(total_price);
        $("#amount_in").attr('value', total_price);

        //Дата
        var day = $('.ui-state-active').html();
        var month = $('.ui-datepicker-month').html();
        var year = $('.ui-datepicker-year').html();
        $("#date_in").attr('value', day+"/"+month+"/"+year);
        
        //Время
        var h_m = $('.block_hours').find('.active').find('span').html();
        $("#time_in").attr('value', h_m);

        //Продолжительность
        var dur = $('.durations').find('.active').find('span').html();
        dur = dur.split(' - ')[0];
        $("#duration_in").attr('value', dur);

        //Дополнительное место
        if ($('#add_place').is('[checked]')){
            $("#add_place_in").attr('value', 'true');
        }else{
            $("#add_place_in").attr('value', '');
        }
    });

  });

$('#add_place').click(function(){
    var price = additional_place();
    if ($(this).is('[checked]')){
        $(this).removeAttr('checked');
        parse_price(price, 'down');
    }else{
        $(this).attr('checked', 'checked');
        parse_price(price, 'up');
    }
});

function additional_place() {
    var price = $('#add_place').next().html().split('место ')[1].split(' ₽')[0];
    price = price.replace(' ', '');
    return price;
}

function ten_persent_place(price){
    //Понадобится для сложения элементов массива
    $('#add_place').removeAttr('disabled');
    var price = parseInt(price);
    price = price*0.1;
    $('#add_place').next('span').html("Дополнительное место "+price+" ₽");
    return true;
}

function parse_price(price, type){
    //Понадобится для сложения элементов массива
    price = parseInt(price)
    var total_price = $(".total_price").html().split(' ')[1];
    total_price += $(".total_price").html().split(' ')[2];
    total_price = parseInt(total_price);
    if (type == 'up'){
        total_price = total_price + price;
    }else{
        total_price = total_price - price;
    }
    total_price = total_price.toString();
    var a = total_price.slice(0,total_price.length - 3);
    var b = total_price.substring(total_price.length-3,total_price.length);
    $('.total_price').html("ИТОГО "+a+" "+b+" ₽");
    return true;
}