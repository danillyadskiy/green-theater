<?php

use Phalcon\Mvc\Model;

class IndexModel extends Model
{

    public static function get_time($year, $month, $day)
    {
        $date = self::parse_date($year, $month, $day);
        $xml = RequestForm::get_time($date);
        return self::parse_time($xml, $date);
    }

    public static function encode_time($time)
    {
        return round((int)$time/24, 6);
    }

    public static function parse_date($year, $m, $day, $add_days="2")
    {
        $m_array= array(
            "Январь" => "01",
            "Февраль" => "02",
            "Март" => "03",
            "Апрель" => "04",
            "Май" => "05",
            "Июнь" => "06",
            "Июль" => "07",
            "Август" => "08",
            "Сентябрь" => "09",
            "Октябрь" => "10",
            "Ноябрь" => "11",
            "Декабрь" => "12",
        );
        $month = $m_array[$m];
        //Считаем количество дней в месяце
        $days_of_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        //Проверяем, если пришло число больше - высылаем след. месяц
        if ($days_of_month-$day < 0){
            //Достаем след. элемент массива
            $my_key = $month;
            $keys = array_keys($m_array);
            $i = array_search($m, $keys);
            $m  = $keys[$i+1];
            $month = $m_array[$m];
            //Первый день след. месяца
            $day = "01";
        }
        $date1=date_create("1900-01-01");
        $date2=date_create("$year-$month-$day");
        $diff=date_diff($date1,$date2);
        return $diff->format("%a")+$add_days;
    }

    public static function parse_time($xml, $date)
    {
        $records = $xml->RESULTS->DIARY->RECORDSET->RECORD;
        $k = 0;
        foreach ($records as $record):
            //Инициализация
            $time_s = (float)$record->STARTTIME;
            $time_f = (float)$record->FINISHTIME;
            $date_s = (int)$record->STARTDATE;
            $date_f = (int)$record->FINISHDATE;
            //Преобразуем (округляем до целых)
            $time_s = round($time_s*24);
            $time_f = round($time_f*24);

            //Совпадает ли выбранный день с днем старта мероприятия в api
            if ($date == $date_s){
                $current_date = true;
            }else{
                $current_date = false;
            }

            //Заносим в массив по условиям
            //1 Проверяем на разрыв даты 28.01
            if ($date_f > $date_s && $k == 0 && !$current_date){
                for($i=0; $i<=$time_f; $i++) {
                    $time[] = $i;
                }
            }
            //2 Просто заносим 
            else if ($time_s<=$time_f){
                for($i=$time_s; $i<=$time_f; $i++) {
                    $time[] = $i;
                }
            //3 Тоже проверка на разрыв даты 27.01
            }else{
                for($i=$time_s; $i<=23; $i++) {
                    $time[] = $i;
                } 
            }
            $k++;
        endforeach;
        //Удаляем дубликаты
        $time = array_values(array_unique($time));
        asort($time);
        return $time;
    }

}