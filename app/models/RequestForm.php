<?php

use Phalcon\Mvc\Model;

class RequestForm extends Model
{
    private static $server ='http://91.221.53.93:1234';
    private static $public_key = '232553529';
    private static $private_key = 'test';

    private static function secret_key(){
        return md5(self::$private_key. self::$public_key);
    }

    protected static function curl($request){
      
        $curl = curl_init(self::$server);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);   
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        $result = curl_exec($curl);
        curl_close($curl);
  
        $result=simplexml_load_string(urldecode($result), "SimpleXMLElement", LIBXML_NOCDATA);
        if ($result->ERRORCODE > 0 || $result == null){
            $view = new Phalcon\Mvc\View();
            return $view->response->redirect("index");
        }else{
            return $result;
        }
    }

    public static function get_time($date)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
            <FUNCNAME>LIST_OF_DIARY</FUNCNAME>
            <PARAMETERS>
                <DATE0>'.$date.'</DATE0>
                <DATE1>'.$date.'</DATE1>
                <SHOW_DIARY_KIND0>1</SHOW_DIARY_KIND0>
                <IDROOM>1</IDROOM>
                <SHOW_GROUP_SERVICES>0</SHOW_GROUP_SERVICES>
            </PARAMETERS>
        </REQUEST>';

        $xml = self::curl($request);
        return $xml;
    }
// Reservation_1 ------------------------------------------------------------------------------------
    public static function add_guest($date)//Тестовое имя | Необходимо сохранить id
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>ADD_GUEST</FUNCNAME>
        <PARAMETERS>
            <ID>0</ID>
            <NAME>'.$date.'</NAME>
            <LOGIN/>
            <PASSWRD/>
        </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

    public static function create_one_diary_abonement($guest_id, $start, $end, $diary_name)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>CREATEONEDIARYABONEMENT</FUNCNAME>
            <PARAMETERS>
                <GUEST>'.$guest_id.'</GUEST>
                <LOGIN/>
                <PASSWRD/>
                <IDROOM>1</IDROOM>
                <DIARY_KIND>0</DIARY_KIND>
                <DATE0>'.$start.'</DATE0>
                <DATE1>'.$end.'</DATE1>
                <STAFF>0</STAFF>
                <DIARY_NAME>'.$diary_name.'</DIARY_NAME>
                <PACKAGE_KIND>12</PACKAGE_KIND>
                <PACKAGE>2</PACKAGE>
            </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

// AfterPAYMENT ------------------------------------------------------------------------------------

    public static function modify_guest($guest_id, $name, $email, $phone)//id, name, email, phone
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>MODIFY_GUEST</FUNCNAME>
        <PARAMETERS>
            <ID>'.$guest_id.'</ID>
            <NAME>'.$name.'</NAME>
            <EMAIL>'.$email.'</EMAIL>
            <PHONES>'.$phone.'</PHONES>
        </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

    public static function diary_comment($diary_id, $comment)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>SQLOPEN</FUNCNAME>
	    <PARAMETERS>
            <SQLTEXT>
            select * from SP_EXECUTE_STATEMENTS(\'update diary d set d.info="'.$comment.'" where d.id='.$diary_id.'\',\'\')
            </SQLTEXT>
	    </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

    public static function folio_payment($guest_id, $amount, $payment_id)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>FOLIO_PAYMENT</FUNCNAME>
	        <PARAMETERS>
		        <GUEST>'.$guest_id.'</GUEST>
                <CARDNO>0</CARDNO>
                <CARDTYPE>2</CARDTYPE>
                <TRACK></TRACK>
                <LOGIN/>
                <PASSWORD/>
                <COST>'.$amount.'</COST>
                <INFO>'.$payment_id.'</INFO>
                <PAYCODE>904</PAYCODE>
	        </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

// SUPPORT----------------------------------------------------------------------------------------------
    public static function guest_info($guest_id)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>GUEST_INFO</FUNCNAME>
        <PARAMETERS>
            <GUEST>'.$guest_id.'</GUEST>
            <CARDNO>0</CARDNO>
            <CARDTYPE>0</CARDTYPE>
            <TRACK>770=12345678=987405</TRACK>
            <LOGIN/>
            <PASSWRD/>
            <SHOWABONEMENTS>1</SHOWABONEMENTS>
            <SHOW_ABONEMENT_PACKAGES>1</SHOW_ABONEMENT_PACKAGES>
            <SHOWDIARY>1</SHOWDIARY>
            <SHOW_DIARY_KIND0>0</SHOW_DIARY_KIND0>
            <SHOW_DIARY_KIND1>0</SHOW_DIARY_KIND1>
            <SHOW_DIARY_KIND2>0</SHOW_DIARY_KIND2>
        </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

    public static function list_of_guests()
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>LIST_OF_GUESTS</FUNCNAME>
        <PARAMETERS>
            <ACCOMPANIED_GUEST>0</ACCOMPANIED_GUEST>
            <FIO_STARTING_WITH>Д</FIO_STARTING_WITH>
            <EMAIL>danillyadskiy@gmail.com</EMAIL>
            <MAXRECORDS>100</MAXRECORDS>
            <FILTER> and 1=1 </FILTER>
            <ORDERBY> g.id desc </ORDERBY>
            <EXTRA_OUTPUT_FIELDS> g.AUX, formatdatetimeex(\'c\',g.REGDATE,\'\') fmtregdate </EXTRA_OUTPUT_FIELDS>
            <EXTRA_OUTPUT_FIELDS_GROUPBY> g.AUX, g.REGDATE </EXTRA_OUTPUT_FIELDS_GROUPBY>
        </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

    public static function info($date_time0, $date_time1)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
            <FUNCNAME>LIST_OF_DIARY</FUNCNAME>
            <PARAMETERS>
                <DATETIME0>'.$date_time0.'</DATETIME0>
                <DATETIME1>'.$date_time1.'</DATETIME1>
                <SHOW_DIARY_KIND0>1</SHOW_DIARY_KIND0>
                <IDROOM>1</IDROOM>
            </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

    public static function abonement_info($abonement_id)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>ABONEMENT_INFO</FUNCNAME>
        <PARAMETERS>
            <ID>82</ID>
            <SHOW_ABONEMENT_PACKAGES>'.$abonement_id.'</SHOW_ABONEMENT_PACKAGES>
            <SHOW_ABONEMENT_PACKAGE_DIARY>0</SHOW_ABONEMENT_PACKAGE_DIARY>
            <SHOW_ABONEMENT_FREEZE>0</SHOW_ABONEMENT_FREEZE>
        </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }

    public static function delete_abonement($guest_id, $abon_id)
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <REQUEST PUBLICKEY="' . self::$public_key . '" SECRETKEY="' . self::secret_key() . '">
        <FUNCNAME>DELETE_ABONEMENTS</FUNCNAME>
        <PARAMETERS>
            <GUEST>'.$guest_id.'</GUEST>
            <LOGIN/>
            <PASSWRD/>
            <ABONEMENTS>
                <RECORDSET>
                    <RECORD>
                        <ID>'.$abon_id.'</ID>
                    </RECORD>
                </RECORDSET>
            </ABONEMENTS>
        </PARAMETERS>
        </REQUEST>';
        $xml = self::curl($request);
        return $xml;
    }
//--------------------------------------------------------------------------------------------------------
}