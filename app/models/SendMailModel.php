<?php
use Phalcon\Mvc\Model;
use Phalcon\Mvc\View;
class SendMailModel extends Model
{

    public function sendEmail($id){
        // Create the Transport
        $transport = (new Swift_SmtpTransport('smtp.yandex.ru', 465, 'ssl'))
        ->setUsername('spa.peshera')
        ->setPassword('SafePeshera1324')
        ;

        //getDataFromModel
        $data = Reservations::findFirst($id);

        // Create a message
        $message = (new Swift_Message('Подтверждение вашего бронирования в Спа Пещера'))
        ->setFrom(['spa.peshera@yandex.ru' => 'Спа Пещера'])
        ->setTo($data->email)
        ;

        //Передача параметра в view
        $logo = $message->embed(Swift_Image::fromPath('./src/assets/img/logo3.png'));

        //Рендер phtml-файла
        $view = new Phalcon\Mvc\View();
        $view->setViewsDir(__DIR__ . '/../views/');
        $view->setVars(
            [
                "logo" => $logo,
                "abonement_id" => $data->abonement_id,
                "name" => $data->name,
                "phone" => $data->phone,
                "email" => $data->email,
                'comment' =>$data->comment,
                "duration" => $data->duration,
                "date" => $data->date,
                "time" => $data->time,
                "amount" => $data->amount,
                "add_place" => $data->add_place,
                "created_at" => $data->created_at,

            ]
        );
        $view->start();
        $view->render('emails', 'reservation');
        $view->finish();
        $content = $view->getRender('emails', 'reservation');

        //phtml-файл
        $message->setBody($content, 'text/html');

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Send the message
        return $mailer->send($message);
    }
}