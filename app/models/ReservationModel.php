<?php
use Phalcon\Mvc\Model;

class ReservationModel extends Model
{
    public static function reservation_1($data)
    {
        //Имя тестового пользователя
        date_default_timezone_set('Europe/Moscow');
        $date = date('d/m/Y H:i:s', time());

        $SE = self::get_start_date_time($data);
        $start = $SE['start'];
        $end = $SE['end'];

        $diary_name = self::diary_name($data);

        //Проверка есть ли бронь в выбранном диапазоне
        $result = RequestForm::info($start, $end);
        if (!$result->RESULTS->DIARY->RECORDSET->RECORD){

            //Если в диапазоне бронь отсутствует
            //Добавляем тестового пользователя
            $guest_id = (string)RequestForm::add_guest($date)->RESULTS->GUEST->ID[0];

            //Создаем на него мероприятие
            $diary_abonement_id = RequestForm::create_one_diary_abonement($guest_id, $start, $end, $diary_name);
            $abonement_id = (string)$diary_abonement_id->RESULTS->ABONEMENT[0];
            $diary_id = (string)$diary_abonement_id->RESULTS->DIARY[0];

            //Сохраняем результат и проверяем его
            $reservations = new Reservations();
            $reservations->abonement_id = $abonement_id;
            $reservations->diary_id = $diary_id;
            $reservations->guest_id = $guest_id;
            if($reservations->save()){
                return $reservations->id;
            }else{
                return 'fail';
            }
        }else{
            //Если в диапазоне уже есть бронь
            return 'fail';
        }
    }

    public static function get_start_date_time($data)
    {
        $date_start  = IndexModel::parse_date($data['year'], $data['month'], $data['day_start'], "3");
        $time_start = IndexModel::encode_time($data['time_start']);
        $date_end = IndexModel::parse_date($data['year'], $data['month'], $data['day_end'], "3");
        $time_end = IndexModel::encode_time($data['time_end']);
        $start = $date_start+$time_start;
        $end = $date_end+$time_end;
        return array('start' => $start,
                     'end' => $end);
    }

    public static function diary_name($data)
    {
        if ($data['add_place'] == 'false'){
            return 'Сауна';
        }else{
            return 'Сауна 7 человек';
        } 
    }

    public static function cancel_reservation($id)
    {

        $reservation = Reservations::findFirst($id);
        $guest_id = $reservation->guest_id;
        $abonement_id = $reservation->abonement_id;

        RequestForm::delete_abonement($guest_id, $abonement_id);

        if($reservation !== false && $reservation->delete()){
            return 'deleted';
        }else{
            return 'fail';
        } 
    }

    public static function cancel_reservation_without_db($id)
    {

        $reservation = Reservations::findFirst($id);
        $guest_id = $reservation->guest_id;
        $abonement_id = $reservation->abonement_id;

        RequestForm::delete_abonement($guest_id, $abonement_id);

        $reservation->status = -1;
        if($reservation->save()){
            return true;
        }else{
            return 'fail';
        }
    }

    public static function modify_guest_diary_folio($id, $payment_id)
    {

        $reservation = Reservations::findFirst($id);
        $guest_id = $reservation->guest_id;
        $diary_id = $reservation->diary_id;
        $name = $reservation->name;
        $email = $reservation->email;
        $phone = $reservation->phone;
        $comment = $reservation->comment;
        $amount = $reservation->amount;
        RequestForm::modify_guest($guest_id, $name, $email, $phone);
        RequestForm::diary_comment($diary_id, $comment);
        RequestForm::folio_payment($guest_id, $amount, $payment_id);
        return true;
    }
}