<?php

use Phalcon\Mvc\Model;

class Helper extends Model
{

    public static function till()
    {
        date_default_timezone_set('UTC');
        return date("Y-m-d\TH:i:s", (int)time()+900); 
    }

}