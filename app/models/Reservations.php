<?php

use Phalcon\Mvc\Model;

class Reservations extends Model
{
    public $id;
    public $abonement_id;
    public $diary_id;
    public $guest_id;
    public $status;
    public $name;
    public $phone;
    public $email;
    public $comment;
    public $duration;
    public $date;
    public $time;
    public $amount;
    public $add_place;
    public $payment_id;
    public $created_at;
}