<?php

class ErrorController extends ControllerBase
{
    public function show404Action()
    {
        // Установка кода статуса
        $this->response->setStatusCode(404, 'Not Found');

        // Установка содержимого ответа
        $this->response->setContent("Сожалеем, но страница не существует");

        // Отправка ответа клиенту
        $this->response->send();

        $this->view->pick('404/404');
    }
}