<?php

use Phalcon\Mvc\View;
use \DataTables\DataTable;

class AdminController extends ControllerBase
{
    public function indexAction() {
        $db_limit = 20000;
        //Если пользователь авторизован
        if($this->cookies->has('logged')) {
            if ($this->request->isAjax()) {
                $builder = $this->modelsManager->createBuilder()
                    ->columns('id, abonement_id, created_at, status, name, phone, 
                                email, duration, date, time, amount, add_place, payment_id')
                    ->from('Reservations')
                    ->where('status != 0');

                $dataTables = new DataTable(['limit' => $db_limit]);
                $dataTables->fromBuilder($builder)->sendResponse();
            }
            $this->view->setLayout('admin');
        }else{
            return $this->response->redirect('admin/signIn'); 
        }
    }

    public function signInAction() {

        //Если пользователь авторизован
        if($this->cookies->has('logged')) {
            return $this->response->redirect('admin');
        }

        //Если пользователь не авторизован и пришел POST
        if($this->request->isPost()) {

            //Логин и пароль админа
            $username = 'SpaPeschera';
            $pass = '963852741';

            //Логин и пароль введеный
            $post_username = $this->request->getPost('username');
            $post_pass = $this->request->getPost('password');
            
            //Если username и password были введены верно
            if ($username == $post_username && $pass == $post_pass){
                $this->cookies->set(
                    'logged',
                    'true',
                    time() + 86400
                );
                $this->cookies->send();
                return $this->response->redirect('admin');
            //Если username и/или password были введены НЕВЕРНО
            }else{
                $this->view->error = 'true';
            }
            
        }
        //Если пользователь не авторизован и НЕ пришел POST
        $this->view->setLayout('index');
        $this->assets->addCss('src/assets/css/sign_in.css');
    }

    public function logoutAction() {
        $cookie = $this->cookies->get('logged');
        $cookie->delete();
        return $this->response->redirect('admin/signIn'); 
    }

    public function sendMailAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );

        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $id = $this->request->getPost('id');
                $mail = new SendMailModel();
                $mail->sendEmail($id);
            }
        }else{
            $this->triggerHttpError(400, 'Bad Request');
            return;
        }
        return;
    }
}