<?php

use Phalcon\Mvc\View;

class CronController extends ControllerBase
{
    public function checkAction()
    {
        date_default_timezone_set('Europe/Moscow');
        /*$dir = dirname(__FILE__);
        $content = file_get_contents($dir."/test.txt");
        $fp = fopen($dir."/test.txt", 'w');*/

        //Начинаем цикл по бд
        $reservations = Reservations::find();
        foreach ($reservations as $res) {
            $dbDateTime = strtotime($res->created_at);
            $currentDateTime = strtotime("now");

            $seconds_diff = $currentDateTime - $dbDateTime;
            $minutes_diff = (int)$seconds_diff/60;

            // Условие если разница во времени больше 15.1 мин и статус оплаты == 0
            //Удаляем запись из БД и API
            if ($minutes_diff > 15.1 && $res->status == 0){
                $response = ReservationModel::cancel_reservation($res->id);
                //fwrite($fp, $res->id.'   '.$response.PHP_EOL);
            }
            // Условие если разница во времени больше 15.1 мин и статус оплаты == 1
            //Удаляем запись из API
            else if ($minutes_diff > 15.1 && $res->status == 1){
                $response = ReservationModel::cancel_reservation_without_db($res->id);
            }        
        }
        //fclose($fp);
    }
}