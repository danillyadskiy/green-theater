<?php

use Phalcon\Mvc\View;

//Форсируем значение по умолчанию created_at
use Phalcon\Db\RawValue;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        if($this->request->isGet()) {
            $data = $this->request->get();
        }

        $this->view->alert = $data['alert'];
        $this->view->display = $data['display'];
        $this->view->icon = $data['icon'];
    }

    public function getTimeAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );

        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $day = $this->request->getPost('day');
                $month = $this->request->getPost('month');
                $year = $this->request->getPost('year');
            }
        }else{
            $this->triggerHttpError(400, 'Bad Request');
            return;
        }
        $response['time_1'] = IndexModel::get_time($year, $month, $day);
        $response['time_2'] = IndexModel::get_time($year, $month, $day+1);
        $this->response->setJsonContent($response);
        return $this->response;
    }

    public function reservationAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );

        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data = $this->request->getPost();
            }
        }else{
            $this->triggerHttpError(400, 'Bad Request');
            return;
        }
        $response = ReservationModel::reservation_1($data);
        $this->response->setJsonContent($response);
        return $this->response;
    }

    public function cancelResAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );

        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $id = $this->request->getPost('id');
            }
        }else{
            $this->triggerHttpError(400, 'Bad Request');
            return;
        }
        $response = ReservationModel::cancel_reservation($id);
        $this->response->setJsonContent($response);
        return $this->response;
    }

    public function payMasterAction()
    {
        if ($this->request->isPost()) {
            $data = $this->request->getPost();
        }
        $res = Reservations::findFirst($data['id']);
        $res->status = 1;
        $res->name = $data['name'];
        $res->phone = $data['phone'];
        $res->email = $data['email'];
        $res->comment = $data['comment'];
        $res->duration = $data['duration'];
        $res->date = $data['date'];
        $res->time = $data['time'];
        $res->amount = $data['amount'];
        $res->add_place = $data['add-place'];
        $res->created_at = new RawValue('default');
        if($res->update() === false){
            return $this->response->redirect('index/failure');  
        }

        //Временная защита от замены HTML
        if($res->amount < 4000){
            ReservationModel::cancel_reservation($res->id);
            return $this->response->redirect('index/failure');  
        }

        return $this->response->redirect("https://paymaster.ru/payment/init/?".join('&',[
            "LMI_MERCHANT_ID=e813e5de-7691-4eb3-80f5-4ddcf236016d",
            "LMI_SHOPPINGCART.ITEMS[0].NAME=Usluga SPA kompleksa",
            "LMI_SHOPPINGCART.ITEMS[0].QTY=1",
            "LMI_SHOPPINGCART.ITEMS[0].PRICE= " . $data['amount'],
            "LMI_SHOPPINGCART.ITEMS[0].TAX=no_vat",
            "LMI_SHOPPINGCART.ITEMS[0].METHOD=1",
            "LMI_SHOPPINGCART.ITEMS[0].SUBJECT=4",
            "LMI_PAYMENT_AMOUNT=" . $data['amount'],
            "LMI_CURRENCY=643",
            "LMI_PAYMENT_DESC=Спа пещера",
            //"LMI_SIM_MODE=1",
            "LMI_PAYER_EMAIL=" . $data['email'],
            "LMI_EXPIRES=" . Helper::till(),
            "id=" . $data['id'],
            ]));
    }

    public function successAction()
    {
        return $this->response->redirect("index?".join('&',[
            'alert=success',
            'display=block',
            'icon=check'
        ]));
    }

    public function failureAction()
    {
        if ($this->request->isPost()) {
            $id = $this->request->getPost('id');
            ReservationModel::cancel_reservation_without_db($id);
        }

        return $this->response->redirect("index?".join('&',[
            'alert=danger',
            'display=block',
            'icon=close-circle-o'
        ]));
    }

    public function apiAction()
    {
        //$this->view->xml_1 = RequestForm::info(43575.916667, 43595.125); //1120, 223
        $this->view->xml_1 = RequestForm::list_of_guests(); //1120, 223
        $this->view->xml_2 = RequestForm::get_time('43572');
    }

}

