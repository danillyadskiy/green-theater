<?php

use Phalcon\Mvc\View;

//Форсируем значение по умолчанию created_at
use Phalcon\Db\RawValue;

class PayMasterController extends ControllerBase
{
    public function paymentNotificationAction()
    {
        $mail = new SendMailModel();
        if ($this->request->isPost()) {
            $id = $this->request->getPost('id');
            $payment_id = $this->request->getPost('LMI_SYS_PAYMENT_ID');

            //После успешной оплаты обновляем статус
            $res = Reservations::findFirst($id);
            $res->status = 2;
            $res->payment_id = $payment_id;
            $res->created_at = new RawValue('default');
            if($res->update() === false){
                return $this->response->redirect('index/failure');  
            }
        
            //Обновляем данные пользователя
            ReservationModel::modify_guest_diary_folio($id, $payment_id);
        
            //Отправляем имэйл
            $mail->sendEmail($id);
        }
        return 'OK';
    }
}